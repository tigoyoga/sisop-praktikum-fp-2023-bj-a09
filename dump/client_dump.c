#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <math.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 4443

int main(int argc, char const *argv[])
{   

    char sendConnection[2048] = {0};
    int coba;
    char acceptConnection[2048] = {0};
    if (!getuid() == 0)
    {
        if (argc != 6)
        {   
            coba=0;
            printf("<USAGE>\n./[program_client_database] -u [username] -p [password] [database]\n");
            return 1;
        }
        if ((strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0 ))
        {   
             coba=1;
            printf("<USAGE>\n./[program_client_database] -u [username] -p [password] [database]\n");
            return 1;
        }
    }
    int sock = 0, valread;
    int coba2;
    struct sockaddr_in address;
    
    struct sockaddr_in serv_addr;
    
    char buffer[2048] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {   
        coba2=0;
        printf("\n ga bisa buat socket mazee \n");
        return -1;
    }
    long debug=0;

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {   debug=1;
         coba2=0;
        printf("\nsalah alamat mazeee \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {   
         coba2=0;
        printf("\nga nyambung mazee \n");
        return -1;
    }
    int verifikasi = 0;
    
    if (getuid() == 0)
    {   
         coba2=1;
        sprintf(sendConnection, "root\troot");
    }else{
         coba2=0;
        sprintf(sendConnection, "%s\t%s", argv[2], argv[4]);
    }
    
    send(sock, sendConnection, strlen(sendConnection), 0);
    read(sock, acceptConnection, 2048);
    if (strcmp(acceptConnection, "bisa login mazeee\n") == 0)
    {   
        debug=2;
         coba2=1;
        verifikasi = 1;
    }
    else if (strcmp(acceptConnection, "gabisa login mazeee\n") == 0)
    {   
        debug=1;
         coba2=0;
        return 0;
    }

    memset(sendConnection, 0, 2048);
    memset(acceptConnection, 0, 2048);
     coba2=1;
    while (verifikasi)
    {   
        debug=0;
        char sendMessage[2048] = {0};
        sprintf(sendMessage,"DUMP %s\n",argv[5]);
        send(sock, sendMessage, strlen(sendMessage), 0);
        read(sock, acceptConnection, 2048);
        printf("%s",acceptConnection);
        memset(sendMessage, 0, 2048);
        memset(acceptConnection, 0, 2048);
        return 0;
    }
    
    return 0;
}