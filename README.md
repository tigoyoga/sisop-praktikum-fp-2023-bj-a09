# sisop-praktikum-fp-2023-bj-A09

Perkenalkan kami dari kelas sistem operasi kelompok A09, dengan anggota sebagai berikut:


|Nama                   |NRP        |
|---|---|
|Muhammad Rifqi Fadhilah|5025211228 |
|Muhammad Naufal Baihaqi|5025211103 |
|Tigo S Yoga            |5025211125 |


*************************************************
## A. AUTENTIKASI

Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.
Catatan: Untuk hak tiap user tidak perlu didefinisikan secara rinci, cukup apakah bisa akses atau tidak.

### `pertama` > menjalankan server

```shell
gcc database.c -o database

./database
```
![](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/194903b43c6eb426110b94b915afd9a8/WhatsApp_Image_2023-06-25_at_21.03.53.jpg)


### `kedua` > membuat user di dalam root

```shee
sudo ./client
```

nanti akan muncul output seperti berikut :
![](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/cb11de6b056de1794159284d40dad791/WhatsApp_Image_2023-06-25_at_21.05.10.jpg)

*user a sudah ada, jadi di situ memakai user b

### `ketiga` > masuk ke dalam user dengan autentikasi username dan password

```shell
./client -u a -p a
```
isinya untuk membuka user a dengan password a

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/c54e6c5d480c58f0b7cdf1db898d51be/WhatsApp_Image_2023-06-25_at_21.06.09.jpg)

*************************************************
## B. AUTORISASI
Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.


### `pertama` > menjalankan server

```shell
./database
```

### `kedua` > masuk ke dalam user

```shell
./client -u a -p a
```

### `ketiga` > membuat database (jika belum ada)

```shell
CREATE DATABASE coba;
```

### `keempat` > masuk ke dalam databasenya untuk melakukan *DML*

```shell
USE coba;
```

### `kelima` > gunakan grant permission jika database ingin digunakan di user lain (dalam case ini tidak dipakai karena penggunaannya hanya ingin di user pemilik database)

> dokumentasinya sebagai berikut :

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/c54e6c5d480c58f0b7cdf1db898d51be/WhatsApp_Image_2023-06-25_at_21.06.09.jpg)
*************************************************

## C. Data Definition Language

### `pertama` > CREATE DATABASE

### `kedua` > CREATE TABLE

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/e724f121bbba0501449aed7501555a4a/WhatsApp_Image_2023-06-25_at_21.07.23.jpg)

## `ketiga` > DROP TABLE

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/674c499894e323b602cf9c0f3dfff968/WhatsApp_Image_2023-06-25_at_21.09.04.jpg)

*************************************************

## D. Data Manipulation Language

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/7d976ddaaf574604ce8e06ae99080968/WhatsApp_Image_2023-06-25_at_21.07.38.jpg)

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/8c48940ea9d4caa1d9129e1866927d26/WhatsApp_Image_2023-06-25_at_21.07.55.jpg)

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/6f765594a9d295c01f60ba123b53b04f/WhatsApp_Image_2023-06-25_at_21.08.21.jpg)

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/e21547cd3a1917044a4a04b71a0cae36/WhatsApp_Image_2023-06-25_at_21.08.47.jpg)


## E. LOGGING

![](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/ec1e58af682eb76c318b55771a1cbb7a/WhatsApp_Image_2023-06-25_at_21.09.31.jpg)

## F. Reliability

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/5fe81aa0d6a37428e771753632c1e07d/WhatsApp_Image_2023-06-25_at_21.10.20.jpg)


JIKA INGIN MEMASUKKANNYA KE DALAM FILE BACKUP:
![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/67964d4af765ebcb07d115dcb07ca009/WhatsApp_Image_2023-06-25_at_21.10.45.jpg)

AKAN KELUAR DI FILE BACKUP SEBAGAI BERIKUT :

![Alt text](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/8fb7bb5a2d360e4c899df4f452f1c4fc/WhatsApp_Image_2023-06-25_at_21.11.31.jpg)

karena saat melakukan backup hanya tersisa tabel `pacar`


## G. Tambahan

...
![](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/2e89b9a573164c4039465816beda628c/WhatsApp_Image_2023-06-25_at_21.43.18.jpg)

## H. Error Handling

Jika ada command yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan error tanpa keluar dari program client.

program sudah disusun agar mengeluarkan pesan error

## I. Containerization

### `pertama` > docker build

proses docker build dilakukan denga menjalankan command berikut :
```shell
sudo docker build -t coba
```

dari command tersebut, `coba` merupakan nama image nya

### `kedua` > docker run

![](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/5b0fcfbb76d445ea919e412e525c89af/WhatsApp_Image_2023-06-25_at_21.12.34.jpg)

![](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/c92bf7b34d6bc478f32b2dffc1458337/WhatsApp_Image_2023-06-25_at_21.12.54.jpg)

sudo docker compose up

![](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/43d42899a65ab20d3d8c82fb0dcdf513/WhatsApp_Image_2023-06-25_at_21.14.31.jpg)

![](https://gitlab.com/tigoyoga/sisop-praktikum-fp-2023-bj-a09/uploads/c9b506ba84cc7f6ba507dd6bb34ac553/WhatsApp_Image_2023-06-25_at_21.15.06.jpg) 


