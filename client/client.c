#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#define PORT 4443

int main(int argc, char const *argv[])
{
    char sendConnection[2048] = {0};
    char acceptConnection[2048] = {0};
    if (!getuid() == 0)
    {
        if (argc != 5)
        {
            printf("Wrong argument !\n./[program_client_database] -u [username] -p [password]\n");
            return 1;
        }
        if ((strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0))
        {
            printf("Wrong argument !\n./[program_client_database] -u [username] -p [password]\n");
            return 1;
        }
    }
    
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    int test2;
    char buffer[2048] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {   
        
        printf("\n Error to create socket!\n");
        return -1;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {   
        printf("\nWrong address !\n");
        return -1;
    }

    /* code */

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {   
        printf("\nCan't Connet to socket ! \n");
        return -1;
    }
    int verifikasi = 0;
    
    if (getuid() == 0)
    {    
        sprintf(sendConnection, "root\troot");
    }else{
        sprintf(sendConnection, "%s\t%s", argv[2], argv[4]);
    }
    
    send(sock, sendConnection, strlen(sendConnection), 0);
    read(sock, acceptConnection, 2048);
    printf("%s", acceptConnection);
    if (strcmp(acceptConnection, "Login Success !\n") == 0)
    {   
        verifikasi = 1;
    }
    else if (strcmp(acceptConnection, "Login Failed !\n") == 0)
    {   
        return 0;
    }
     
    memset(sendConnection, 0, 2048);
    memset(acceptConnection, 0, 2048);
     
    while (verifikasi)
    {   

        printf(">> ");
        gets(sendConnection);
        if (strcmp(sendConnection,"") ==0)
        {   
            strcpy(sendConnection,"dummy");
        }
        
        send(sock, sendConnection, strlen(sendConnection), 0);
        read(sock, acceptConnection, 2048);
        printf("%s", acceptConnection);

        if (strcmp(acceptConnection, "Exit Program\n") == 0)
        {   
            return 0;
        }
        memset(sendConnection, 0, 2048);
        memset(acceptConnection, 0, 2048);
    }

    return 0;
}
