#!/bin/bash

# Pindah ke direktori tempat file .backup berada
cd /home/tigoyoga/Documents/kuliah/fp-sisop/fp-sisop-bj-A09/dump

# Membuat zip dengan nama jam_hari.zip
zip "$(date +%H_%d).zip" *.backup

# Menghapus file .backup setelah di-zip
rm -f *.backup
