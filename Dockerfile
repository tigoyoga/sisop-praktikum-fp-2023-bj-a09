# # Menggunakan image base Linux
# FROM ubuntu:latest

# # Mengatur direktori kerja
# WORKDIR /storage-app

# # Meng-copy file-file yang diperlukan ke dalam container
# COPY ./database/database.c /storage-app/database.c
# COPY ./client/client.c /storage-app/client.c

# # Menginstal kompiler C
# RUN apt-get update && apt-get install -y gcc

# # Mengompilasi file server.c menjadi binary server
# RUN gcc -o database database.c -lpthread

# # Mengompilasi file client.c menjadi binary client
# RUN gcc -o client client.c -lpthread

# # Menjalankan server dan client saat container berjalan
# CMD ["bash", "-c", "./database & ./client"]

# Menggunakan image base Linux
FROM ubuntu:latest

# Mengatur direktori kerja
WORKDIR /storage-app

# Menginstal kompiler C
RUN apt-get update && apt-get install -y gcc

# Menjalankan perintah untuk membuat direktori
RUN mkdir -p /storage-app/Sukolilo
RUN mkdir -p /storage-app/Mulyos
RUN mkdir -p /storage-app/Gebang
RUN mkdir -p /storage-app/Keputih
RUN mkdir -p /storage-app/Semolowaru
# Tambahkan perintah untuk membuat direktori lainnya sesuai kebutuhan

# Meng-copy file-file yang diperlukan ke dalam container
COPY ./database/database.c /storage-app/database.c
COPY ./client/client.c /storage-app/client.c
# Copy file-file lainnya sesuai kebutuhan dan direktori yang sesuai

# Mengompilasi file server.c menjadi binary server
RUN gcc -o database database.c -lpthread

# Mengompilasi file client.c menjadi binary client
RUN gcc -o client client.c -lpthread

# Menjalankan server dan client saat container berjalan
CMD ["bash", "-c", "./database & ./client"]

